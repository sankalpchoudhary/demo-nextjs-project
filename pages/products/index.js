import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import Navbar from '../_partials/Navbar'

const Products = () => {
    const [products, setproducts] = useState([])

    useEffect(() => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(products => setproducts(products))
    }, [])

    return (
        <section className="text-gray-600 body-font">
            <Navbar />
            <div className="container px-5 mx-auto">
                <div className="flex flex-wrap -m-4">
                    {products.map((product) => {
                        return (
                            <div className="lg:w-1/4 md:w-1/2 p-4 w-full" key={product.key} >
                                <Link href={`/products/${product.id}`}>
                                    <a className="block relative h-48 rounded overflow-hidden">
                                        <Image alt="product picture"
                                            className="object-cover object-center w-full h-full block"
                                            src={product.image}
                                            width={500}
                                            height={500} />
                                    </a>
                                </Link>
                                <div className="mt-4">
                                    <h3 className="text-gray-500 text-xs tracking-widest title-font mb-1">{product.title}</h3>
                                    <h2 className="text-gray-900 title-font text-lg font-medium">{product.category}</h2>
                                    <p className="mt-1">${product.price}</p>
                                </div>
                            </div>

                        )
                    })}
                </div>
            </div>
        </section>
    )
}

export default Products