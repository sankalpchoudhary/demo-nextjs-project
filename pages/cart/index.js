import React, { useEffect, useState } from 'react'
import Navbar from '../_partials/Navbar'
import Link from 'next/link'
import Image from 'next/image'

const Cart = () => {
    const [cart, setCart] = useState([])
    const [products, setProduct] = useState([])


    useEffect(() => {
        fetch('https://fakestoreapi.com/carts/1')
            .then(res => res.json())
            .then(cart => {
                for (let index = 0; index < cart.products.length; index++) {
                    const element = cart.products[index];
                    fetch(`https://fakestoreapi.com/products/${element.productId}`)
                        .then(res => res.json())
                        .then(product_data => setProduct(products => [...products, product_data]))
                }
                setCart(cart)
            })
    }, [])

    return (
        <div>
            <section className="text-gray-600 body-font">
                <Navbar />
                <h1 className='text-center text-3xl' >Cart</h1>
                <div className="container px-5 mx-auto mt-16">
                    <div className="flex flex-wrap -m-4">
                        {products.map((product) => {
                            return (
                                <div class="flex items-center lg:w-3/5 mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col" key={product.key}>
                                    <div class="sm:w-32 sm:h-32 h-20 w-20 sm:mr-10 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 flex-shrink-0">
                                        <Image alt="product picture"
                                            className="object-cover object-center w-full h-full block"
                                            src={product.image}
                                            width={500}
                                            height={500} />
                                    </div>
                                    <div class="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                                        <h2 class="text-gray-900 text-lg title-font font-medium mb-2">{product.title}</h2>
                                        <p class="leading-relaxed text-base">{product.description}</p>
                                        <a class="mt-3 text-indigo-500 inline-flex items-center">Buy Now
                                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                                                <path d="M5 12h14M12 5l7 7-7 7"></path>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                                // <div className="lg:w-1/4 md:w-1/2 p-4 w-full" key={product.key} >
                                //     <Link href={`/products/${product.id}`}>
                                //         <a className="block relative h-48 rounded overflow-hidden">
                                //             <Image alt="product picture"
                                //                 className="object-cover object-center w-full h-full block"
                                //                 src={product.image}
                                //                 width={500}
                                //                 height={500} />
                                //         </a>
                                //     </Link>
                                //     <div className="mt-4">
                                //         <h3 className="text-gray-500 text-xs tracking-widest title-font mb-1">{product.title}</h3>
                                //         <h2 className="text-gray-900 title-font text-lg font-medium">{product.category}</h2>
                                //         <p className="mt-1">${product.price}</p>
                                //     </div>
                                // </div>

                            )
                        })}
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Cart