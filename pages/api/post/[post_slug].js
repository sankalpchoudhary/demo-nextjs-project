// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
  if (req.method === 'GET') {
    // Process a POST request
    const { post_slug } = req.query
    res.status(200).json({ name: post_slug })
  }
}
